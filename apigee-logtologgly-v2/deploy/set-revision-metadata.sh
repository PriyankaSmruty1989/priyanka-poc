# $1: env
# $2: apigee username
# $3: apigee password

echo curl -X POST -d @commit-info "https://dev-api.morrisons.com/deployment/v1/environments/$1/revisions/@last/metadata" -H "Content-Type: application/xml" --data-binary "@commit-info" -u $2:$3
curl -X POST -d @commit-info "https://dev-api.morrisons.com/deployment/v1/environments/$1/revisions/@last/metadata" -H "Content-Type: application/xml" --data-binary "@commit-info" -u $2:$3
# curl -X PUT "https://dev-api.morrisons.com/deployment/v1/environments/$1/revisions/@last/metadata" -H "Content-Type: application/xml" --data-binary "@commit-info" -u $2:$3