commit_hash=$(git log -1 --pretty=format:"%h")
committer=$(git log -1 --pretty=format:"%ce")
commit_date=$(git log -1 --pretty=format:"%cd")
tags=$(git tag --contains HEAD)

echo "<metadata>" > commit-info
echo "	<sharedflow>logtologgly-v2</sharedflow>" >> commit-info
echo "	<commit_hash>$commit_hash</commit_hash>" >> commit-info
echo "	<committer>$committer</committer>" >> commit-info
echo "	<commit_date>$commit_date</commit_date>" >> commit-info
echo "	<tags>$tags</tags>" >> commit-info
echo "</metadata>" >> commit-info

cat commit-info